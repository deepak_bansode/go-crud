package handlers

import (
	"deepak_bansode/go-crud/internal/app/domain"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type RequestBody struct {
	Name string `json:"name"`
	Position  string    `json:"position"`
	Salary  float64    `json:"salary"`
}


type EmployeeID struct {
	CreatedEmployeeID int64 `json:"createdEmployeeID"`
	
}

func CreateEmployeeHandler(employeeUseCase domain.EmployeeUseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//parse request
	var requestBody RequestBody
	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		http.Error(w, "Failed to parse JSON request body", http.StatusBadRequest)
		return
	}
		//call the usecase
		employeeID, err := employeeUseCase.CreateEmployee(requestBody.Name, requestBody.Position, requestBody.Salary)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError) //proper error code return
			return
		}
		emplyeeDetails := EmployeeID{CreatedEmployeeID: employeeID} 
		employeeJson, err := json.Marshal(emplyeeDetails)
		if err != nil {
			http.Error(w, "Failed to encode JSON", http.StatusInternalServerError)
			return
		}
		//return the response
		w.WriteHeader(http.StatusOK)
		w.Write(employeeJson)
	}
}


func GetEmployeeHandler(employeeUseCase domain.EmployeeUseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//parse request
		vars := mux.Vars(r)
		id := vars["id"]
		employeeID, _ := strconv.Atoi(id)
		//call the usecase
		employee, err := employeeUseCase.GetEmployeeByID(int64(employeeID))
		if err != nil {
			http.Error(w, "Failed to get employee details", http.StatusInternalServerError)
			return
		}
		//useCase.CreateEmployee()
		//return the response
		employeeJSON, err := json.Marshal(employee)
		if err != nil {
			http.Error(w, "Failed to encode JSON", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write(employeeJSON)
	}
}


func DeleteEmployeeHandler(employeeUseCase domain.EmployeeUseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//parse request
		vars := mux.Vars(r)
		id := vars["id"]
		employeeID, _ := strconv.Atoi(id)
		//call the usecase
		err := employeeUseCase.DeleteEmployeeByID(int64(employeeID))
		if err != nil {
			http.Error(w, "Failed to delete employee", http.StatusInternalServerError)
			return
		}
		//useCase.CreateEmployee()
		//return the response
		
		w.WriteHeader(http.StatusOK)
		
	}
}



func UpdateEmployeeHandler(employeeUseCase domain.EmployeeUseCase) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		//parse request
	var requestBody RequestBody
	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		http.Error(w, "Failed to parse JSON request body", http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
		id := vars["id"]
		employeeID, _ := strconv.Atoi(id)
		//call the usecase
		err := employeeUseCase.UpdateEmployee(int64(employeeID),requestBody.Name, requestBody.Position, requestBody.Salary)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError) //proper error code return
			return
		}
		
		//return the response
		w.WriteHeader(http.StatusOK)
		
	}
}