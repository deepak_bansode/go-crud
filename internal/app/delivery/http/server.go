package http

import (
	repository "deepak_bansode/go-crud/internal/app/repository/inmemory"
	usecase "deepak_bansode/go-crud/internal/app/usecases"
	"deepak_bansode/go-crud/internal/config"
	utils "deepak_bansode/go-crud/internal/pkg"
	"fmt"
	"net/http"
)

type Server struct {
	Configuration *config.WebServerConfig
	Router        *Router
}

func NewServer(config *config.WebServerConfig) *Server {
	server := &Server{
		Configuration: config,
		Router:        NewRouter(),
	}
	return server
}

func RunServer() (err error) {
	config, err := config.FromEnv()
	if err != nil {
		fmt.Println("Failed to read config")
	}

	//repo
	repository.NewEmployeeInMemoryRepo()
	employeeRepo := repository.GetEmployeeInMemoryRepo()
	repository.NewIdGeneratorInMemoryRepo()
	idGeneratorRepo := repository.GetIdGeneratorInMemoryRepo()
	//use case

	usecase.NewEmployeeUseCase(&employeeRepo, &idGeneratorRepo)
	employeeUseCase := usecase.GetEmployeeUseCase()
	server := NewServer(config)

	server.Router.InitializeRouter(config.RoutePrefix, employeeUseCase)

	cors := utils.CORSSetup()
	fmt.Println("Starting HTTP server on port", config.Port)
	err = http.ListenAndServe(":"+config.Port, cors.Handler(server.Router))
	if err != nil {
		return err
	}
	return nil
}
