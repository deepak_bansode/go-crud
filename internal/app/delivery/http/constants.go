package http

const (
	HealthCheckURI = "/v1/healthcheck"
	EmployeeURI = "/v1/employee"
	GetEmployeeURI = "/v1/employee/{id}"
	DeleteEmployeeURI = "/v1/employee/{id}"
	UpdateEmployeeURI = "/v1/employee/{id}"
)
