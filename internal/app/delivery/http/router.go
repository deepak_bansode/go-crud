package http

import (
	"deepak_bansode/go-crud/internal/app/delivery/http/handlers"
	"deepak_bansode/go-crud/internal/app/domain"
	"net/http"

	"github.com/gorilla/mux"
)

type Router struct {
	*mux.Router
}

func NewRouter() *Router {
	return &Router{mux.NewRouter()}
}

func (r *Router) InitializeRouter(routePrefix string, employeeUseCase domain.EmployeeUseCase) {
	subRoute := (*r).PathPrefix(routePrefix).Subrouter()

	subRoute.HandleFunc(HealthCheckURI, handlers.HealthCheckHandler())
	subRoute.HandleFunc(GetEmployeeURI, handlers.GetEmployeeHandler(employeeUseCase)).Methods(http.MethodGet)
	subRoute.HandleFunc(EmployeeURI, handlers.CreateEmployeeHandler(employeeUseCase)).Methods(http.MethodPost)
	subRoute.HandleFunc(DeleteEmployeeURI, handlers.DeleteEmployeeHandler(employeeUseCase)).Methods(http.MethodDelete)
	subRoute.HandleFunc(UpdateEmployeeURI, handlers.UpdateEmployeeHandler(employeeUseCase)).Methods(http.MethodPut)
}
