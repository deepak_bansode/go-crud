package repository

import (
	"deepak_bansode/go-crud/internal/app/domain"
	"sync"
)

type idGeneratorInMemoryRepo struct{
	id int64
}

var idGeneratorInMemoryRepoOnce sync.Once
var idGeneratorInMemoryRepoInst domain.IDGenerator

func NewIdGeneratorInMemoryRepo() {
	idGeneratorInMemoryRepoOnce.Do(func() {
		idGeneratorInMemoryRepoInst = &idGeneratorInMemoryRepo{
			id: 0,
		}
	})

}

func GetIdGeneratorInMemoryRepo() domain.IDGenerator {
	if idGeneratorInMemoryRepoInst == nil {
		panic("idGeneratorInMemoryRepoInst is not intialized")
	}
	return idGeneratorInMemoryRepoInst
}


func (i *idGeneratorInMemoryRepo)GetID() int64{
	i.id = i.id + 1
	return i.id
}
