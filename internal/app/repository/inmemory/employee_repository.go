package repository

import (
	"deepak_bansode/go-crud/internal/app/domain"
	"errors"
	"sync"
)

type employeeInMemoryRepo struct{
	employees sync.Map
}

var employeeInMemoryRepoOnce sync.Once
var employeeInMemoryRepoInst domain.EmployeeRepository

func NewEmployeeInMemoryRepo() {
	employeeInMemoryRepoOnce.Do(func() {
			employeeInMemoryRepoInst = &employeeInMemoryRepo{}
	})

}

func GetEmployeeInMemoryRepo() domain.EmployeeRepository {
	if employeeInMemoryRepoInst == nil {
		panic("employeeInMemoryRepoInst is not intialized")
	}
	return employeeInMemoryRepoInst
}

func (e *employeeInMemoryRepo)SaveEmployee(employee *domain.Employee) error{
	e.employees.Store(employee.ID, employee)
	return nil
}


func (e *employeeInMemoryRepo)GetEmployeeByID(id int64) (*domain.Employee, error){
	if e, ok := e.employees.Load(id); ok{
		employee:= e.(*domain.Employee)
		return employee, nil
	}
	return nil, errors.New("employee not found")
}


func (e *employeeInMemoryRepo) UpdateEmployee(newEmployeeDetails *domain.Employee) error{
	e.employees.Store(newEmployeeDetails.ID, newEmployeeDetails)
	return nil
}

func (e *employeeInMemoryRepo) DeleteEmployeeByID(id int64) error{
	e.employees.Delete(id)
	return nil
}