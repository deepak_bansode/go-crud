package domain

type Employee struct {
	ID       int64
	Name     string
	Position string
	Salary   float64
}

func NewEmployee(id int64, name string, position string, salary float64) *Employee {
	return &Employee{
		ID:       id,
		Name:     name,
		Position: position,
		Salary:   salary,
	}
}


type EmployeeUseCase interface {
	CreateEmployee(name string, position string, salary float64) (int64, error)
	GetEmployeeByID(employeeID int64) (*Employee, error)
	UpdateEmployee(id int64, name string, position string, salary float64) error
	DeleteEmployeeByID(id int64) error
}