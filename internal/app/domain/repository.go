package domain

type EmployeeRepository interface {
	SaveEmployee(employee *Employee) error
	GetEmployeeByID(id int64) (*Employee, error)
	UpdateEmployee(newEmployeeDetails *Employee) error
	DeleteEmployeeByID(id int64) error
}

type IDGenerator interface {
	GetID() int64
}
