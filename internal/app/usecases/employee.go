package usecase

import (
	"deepak_bansode/go-crud/internal/app/domain"
	"errors"
	"fmt"
	"sync"
)


type employeeUseCase struct {
	employeeRepository *domain.EmployeeRepository
	idGenerator        *domain.IDGenerator
}

var employeeUseCaseOnce sync.Once
var employeeUseCaseInst domain.EmployeeUseCase

func NewEmployeeUseCase(
	employeeRepository *domain.EmployeeRepository,
	idGenerator *domain.IDGenerator) {

	employeeUseCaseOnce.Do(func() {
		employeeUseCaseInst = &employeeUseCase{
			employeeRepository: employeeRepository,
			idGenerator:        idGenerator,
		}
	})

}

func GetEmployeeUseCase() domain.EmployeeUseCase {
	if employeeUseCaseInst == nil {
		panic("employeeUseCaseInst is not intialized")
	}
	return employeeUseCaseInst
}

func (e *employeeUseCase) CreateEmployee(
	name string,
	position string,
	salary float64) (int64, error) {
	if name == "" || position == "" || salary == 0 {
		return 0, errors.New("missing data")
	}
	newEmployeeID := (*e.idGenerator).GetID()
	employee := domain.NewEmployee(newEmployeeID, name, position, salary)
	err := (*e.employeeRepository).SaveEmployee(employee)
	if err != nil {
		fmt.Println("Failed to save the employee")
	}
	return newEmployeeID, nil
}

func (e *employeeUseCase) GetEmployeeByID(
	employeeID int64) (*domain.Employee, error) {
	employee, err := (*e.employeeRepository).GetEmployeeByID(employeeID)
	if err != nil {
		fmt.Println("Failed to save the employee")
		return nil, err
	}
	return employee, nil
}

func (e *employeeUseCase) UpdateEmployee(
	id int64,
	name string,
	position string,
	salary float64) error {
	newEmployeeDetails := domain.NewEmployee(id, name, position, salary)
	err := (*e.employeeRepository).UpdateEmployee(newEmployeeDetails)
	if err != nil {
		fmt.Println("Failed to udpdate the employee")
		return err
	}
	return nil
}

func (e *employeeUseCase) DeleteEmployeeByID(
	id int64) error {
	err := (*e.employeeRepository).DeleteEmployeeByID(id)
	if err != nil {
		fmt.Println("Failed to delete the employee")
		return err
	}
	return nil
}
