package config

import (
	"fmt"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type WebServerConfig struct {
	Port        string `required:"true" split_words:"true" default:"3000"`
	RoutePrefix string `required:"false" split_words:"true" default:"/go-crud"`
}

func FromEnv() (c *WebServerConfig, err error) {
	configFileName := "../../etc/config.localhost.env"

	err = godotenv.Load(configFileName)
	if err != nil {
		fmt.Println("No config file found to load env.")
		return
	}

	c = &WebServerConfig{}

	err = envconfig.Process("", c)
	if err != nil {
		fmt.Println("Failed to process env")

	}
	return
}
