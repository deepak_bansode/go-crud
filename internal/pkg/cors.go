package pkg

import (
	"net/http"

	"github.com/rs/cors"
)

func CORSSetup(allowedOrigins ...string) *cors.Cors {
	if len(allowedOrigins) == 0 {
		allowedOrigins = append(allowedOrigins, "*")
	}

	return cors.New(cors.Options{
		AllowedOrigins: allowedOrigins,
		AllowedHeaders: []string{
			"Accept",
			"Content-Type",
			"Authorization",
			"X-CSRF-Token",
			"access-control-allow-origin",
			"Content-Length",
		},
		AllowCredentials: true,
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodDelete,
			http.MethodPatch,
			http.MethodPut,
			http.MethodOptions},
	})
}
