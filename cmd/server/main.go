package main

import "deepak_bansode/go-crud/internal/app/delivery/http"

func main() {
	err := http.RunServer()
	if err != nil {
		panic(err)
	}
}
